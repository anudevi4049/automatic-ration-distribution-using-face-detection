#include <SPI.h>
#include <Servo.h>
#include <HX711_ADC.h>
#include <EEPROM.h>
const int HX711_dout = 4; //mcu > HX711 dout pin
const int HX711_sck = 5; //mcu > HX711 sck pin
#define SS_PIN 10
#define RST_PIN 9
#define LED_G 4 //define green LED pin
#define LED_R 5 //define red LED
#define BUZZER 2 //buzzer pin
HX711_ADC LoadCell(HX711_dout, HX711_sck);
Servo myServo; //define servo name
const int calVal_eepromAdress = 0;
unsigned long t = 0;
int val = 10;
char d;
void setup()
{
Serial.begin(9600); // Initiate a serial communication
SPI.begin(); // Initiate SPI bus
myServo.attach(3); //servo pin
myServo.write(0); //servo start position
pinMode(LED_G, OUTPUT);
pinMode(LED_R, OUTPUT);
pinMode(BUZZER, OUTPUT);
noTone(BUZZER)
LoadCell.begin();
float calibrationValue; // calibration value (see example file "Calibration.ino")
calibrationValue = 696.0; // uncomment this if you want to set the calibration value in the sketch
#if defined(ESP8266)|| defined(ESP32)
//EEPROM.begin(512); // uncomment this if you use ESP8266/ESP32 and want to fetch the calibration value from eeprom
#endif
EEPROM.get(calVal_eepromAdress, calibrationValue); // uncomment this if you want to fetch the calibration value from eeprom
unsigned long stabilizingtime = 2000; // preciscion right after power-up can be improved by adding a few seconds of stabilizing time
boolean _tare = true; //set this to false if you don't want tare to be performed in the next step
LoadCell.start(stabilizingtime, _tare);
if (LoadCell.getTareTimeoutFlag()) {
Serial.println("Timeout, check MCU>HX711 wiring and pin designations");
while (1);
}
else {
LoadCell.setCalFactor(calibrationValue); // set calibration value (float)
Serial.println("Startup is complete");
}
}
void loop()
{
if(Serial.available())
{
d = Serial.read();
printf("assigned value to d");
}
if  d==’a’)
{
Serial.println("Authorized access");
Serial.println();
delay(500);
Serial.println("1st delay");
tone(BUZZER, 500);
delay(500);
Serial.println("2nd delay");
noTone(BUZZER);
myServo.write(180);
Serial.println("servo started");
delay(5000);
Serial.println("3rd delay");
//Serial.println(newDataReady);
while(1)
{
static boolean newDataReady = 0;
const int serialPrintInterval = 0;
if (LoadCell.update()) newDataReady = true;
Serial.println("in the loop")
if (newDataReady) {
if(millis() > t + serialPrintInterval) {
float i = LoadCell.getData();
Serial.print("Load_cell output val: ");
Serial.println(i);
newDataReady = 0;
t = millis();
if(i>490)
{
break;
}
}
}
}
myServo.write(0);
Serial.println("servo back to zero");
digitalWrite(LED_G, LOW);
if (Serial.available() > 0) {
char inByte = Serial.read();
if (inByte == 't') LoadCell.tareNoDelay();
}
// check if last tare operation is complete:
if (LoadCell.getTareStatus() == true) {
Serial.println("Tare complete");
} }
else {
Serial.println(" Access denied");
digitalWrite(LED_R, HIGH);
tone(BUZZER, 300);
delay(1000);
digitalWrite(LED_R, LOW);
noTone(BUZZER);
}
}
